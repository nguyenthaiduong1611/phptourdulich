<?php require './config/constants.php';?>
<html>
    <head>
        <link rel="stylesheet" href="./tkall.css">
        <link rel="stylesheet" href="./fontawesome-free-5.15.4-web/css/all.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">
    </head>
    <body>
        <div id="viewport">
            <!-- Sidebar -->
            <div id="sidebar">
              <header>
                <a href="#">Quản lí website</a>
              </header>
              <ul class="nav">
                <li>
                  <a href="http://localhost/tour-order/admin/trangchu.php">
                    <i class="fas fa-home"></i>Quản lý admin
                  </a>
                </li>
                <li>
                  <a href="http://localhost/tour-order/admin/quanlytour.php">
                    <i class="fas fa-tasks"></i>Quản lý du lịch  
                  </a>
                </li>
                <li>
                  <a href="http://localhost/tour-order/admin/quanlydlbien.php">
                  <i class="fas fa-tasks"></i>Quản lý du lịch biển
                  </a>
                  <ul class="travel-sea">
                    <li><a href="http://localhost/tour-order/admin/ql-bb.php">Du lịch bờ biển</a></li>
                    <li><a href="http://localhost/tour-order/admin/ql-dt.php">Du lịch du thuyền</a></li>
                  </ul>
                </li>
                <li>
                  <a href="http://localhost/tour-order/admin/quanlyhd.php">
                    <i class="fas fa-tasks"></i>Quản Lý Hóa Đơn
                  </a>
                </li>
                <li>
                  <a href="http://localhost/tour-order/admin/sukien.php">
                    <i class="far fa-calendar-alt"></i> Quản Lí Sự Kiện
                  </a>
                </li>
                <li>
                  <a href="http://localhost/tour-order/admin/qldanhmuc.php">
                    <i class="far fa-calendar-alt"></i> Quản Lí Danh Mục
                  </a>
                </li>
                <li>
                  <a href="http://localhost/tour-order/admin/qlkh.php">
                    <i class="far fa-calendar-alt"></i> Quản Lí Khách Hàng
                  </a>
                </li>
                <li>
                  <a href="http://localhost/tour-order/admin/tk.php">
                    <i class="fas fa-chart-line"></i> Thống Kê
                  </a>
                </li>
                <li>
                  <a href="http://localhost/tour-order/admin/login.php">
                    <i class="fas fa-sign-out-alt"></i>Đăng xuất
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div id="content">
            <nav class="navbar navbar-default">
              <div class="container-fluid active">
                  <div class="header">
                  <div class="manage-admin">
                      <div>
                          <div style="margin-left:150px; margin-top: 100px;">
                              <h3>Thống Kê </h3>
                          </div>
                      </div>
                      
                      <div class="thongke">
                        <div>
                          <h3>Tổng hóa đơn </h3>
                          <p>
                            <?php
                              if (mysqli_connect_error())
                              {
                              echo "Failed to connect to MySQL: " . mysqli_connect_error();
                              }
                              $sql="SELECT count('STT') from tbl_hoadon";
                              $result=mysqli_query($conn,$sql);
                              $row=mysqli_fetch_array($result);
                              echo "$row[0]";
                              mysqli_close($conn);
                            ?>
                          </p>
                        </div>
                        <div>
                          <h3>Tổng hóa đơn </h3>
                          <p>
                            <?php
                              // if (mysqli_connect_error())
                              // {
                              // echo "Failed to connect to MySQL: " . mysqli_connect_error();
                              // }
                              // $sql="SELECT gia   from tbl_hoadon";
                              // $result = mysqli_query($conn,$sql);
                              // $row=mysqli_fetch_array($result);
                              // echo "$row[0]";
                              // mysqli_close($conn);

                              if (mysqli_connect_error())
                              {
                              echo "Failed to connect to MySQL: " . mysqli_connect_error();
                              }
                              $sql="SELECT SUM(gia) from tbl_hoadon";
                              // $result=mysqli_query($conn,$sql);
                              // $row=mysqli_fetch_array($result);
                              // echo "$row[0]";
                              // mysqli_close($conn);
                            ?>
                          </p>
                        </div>
                      </div>
                  </div>
              </div>
            </nav>
          </div>
    </body>
</html>