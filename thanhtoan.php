
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./thanhtoan.css">
    <link href="stylesheet" href="./fontawesome-free-5.15.4-web/">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

</head>
<body>
  <div class="box">
      <?php require './menu.php';?>  
            
            <div class="db">
                <form action="thanhtoan_post.php" method="POST">
                  <h2>Thông Tin Khách Hàng</h2>
                    <div class="ht">
                    <label for="fname">Họ và Tên:</label>
                    <input type="text" id="fname" name="fullname">
                    </div>
                    <div class="sdt">
                        <label for="fnsdt">Số Điện Thoại Liện Hệ:</label>
                        <input type="text" id="fnsdt" name="SDT">
                    </div>
                    <div class="cmnd">
                        <label for="fncmnd">CMND/CCCD:</label>
                        <input type="text" id="fncmnd" name="CCCD">
                    </div>
                    <div class="cmnd">
                        <label for="fncmnd">Thời Gian Bắt Đầu - Kết Thúc:</label>
                        <input type="text" id="fncmnd" name="ngaykhuhoi">
                    </div>
                    <div class="cmnd">
                        <label for="fncmnd">Số Lượng Thành Viên:</label>
                        <input type="number" id="fncmnd" name="people">
                    </div>
                  <div>
                  <?php
                            $matour = $_GET['matour'];

                            $sql = "SELECT * FROM tbl_travel WHERE matour=$matour";
                          
                            $res = mysqli_query($conn, $sql);

                            if($res==TRUE)
                            {
                                $count = mysqli_num_rows($res);

                                if($count==1)
                                {
                                    $row = mysqli_fetch_assoc($res);

                                    $madm = $row['madm'];
                                    $tentour = $row['tentour'];
                                    $thongtin = $row['thongtin'];
                                    $gia = $row['gia'];
                                    $matour = $row['matour'];
                                    $thoigian = $row['thoigian'];
                                }
                                
                                else
                                {
                                    die();
                                }
                            }
                          ?>
                    <div class="tour">
                        <h2>Thông Tin Địa Điểm Du Lịch</h2>
                        
                        <div class="tentour">
                            <label for="fntt">Mã Tour:</label>
                            <input type="text" id="fntt" name="matour" value="<?php echo $matour;?>" readonly="readonly">
                        </div>
                        <div class="tentour">
                            <label for="fntt">Tên Địa Điểm:</label>
                            <input type="text" id="fntt" name="tentour" value="<?php echo $tentour;?>" readonly="readonly">
                        </div>
                        <div class="timedl">
                            <label for="fntime">Thời gian du lịch:</label>
                            <input type="text" id="fntimedl" name="thoigian" value="<?php echo $thoigian;?>" readonly="readonly">
                        </div>
                        
                        <div class="gia">
                            <label for="fngia">Số tiền cần thanh toán:</label>
                            <input type="text" id="fngia" name="gia" value="<?php echo $gia;?>" readonly="readonly">
                        </div>
                    </div>
                  </div>  
              
                    
                    <div class="form-group">
                        <input type="submit" name="submit" value="Đặt">
                     </div>
                  </form>
            </div>
          </div> 
        </div>
      </div>  
            </div>
            <?php require './footer.php'; ?>

        <?php
    // if(isset($_POST['submit']))
    // {
    //     $matour = $_POST['matour'];
    //     $tentour = $_POST['tentour'];
    //     $gia = $_POST['gia'];
    //     $thoigian = $_POST['thoigian'];
    //     $CCCD = $_POST['CCCD'];
    //     $fullname = $_POST['fullname'];
    //     $SDT = $_POST['SDT'];
    //     $people = $_POST['people'];
    //     $time = $_POST['time'];
    //     $sql = "INSERT INTO tbl_hoadon (fullname,SDT,tentour,thoigian,gia,matour,CCCD,people,'time') VALUES ('".$fullname."','".$SDT."','".$tentour."','".$thoigian."','".$gia."','".$matour."','".$CCCD."','".$people."','".$time."')";
    //     // $sql2 =  "INSERT INTO tbl_hoadon SET
    //     //     fullname = '$fullname',
    //     //     SDT = $SDT, 
    //     //     tentour = '$tentour', 
    //     //     thoigian = '$thoigian', 
    //     //     time = '$time', 
    //     //     gia = $gia, 
    //     //     people = $people, 
    //     //     matour = $matour,
    //     //     CCCD = $CCCD
            
    //     // ";
    //     $res2 = mysqli_query($conn, $sql) ;
    //     if($res2==TRUE)
    //     {
    //         header('location:'.SITEURL.'trangchu.php');
    //     }
    //     else
    //     {
    //       die();
    //     }
    // }
    
?>