<?php require './admin/config/constants.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./dlbien.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">
    <title>TOUR DU LỊCH VIỆT NAM - DU LỊCH MINHTUS</title>
</head>
<body>
    <div class="container">
      <?php require './menu.php';?>

      
        <div class="img-sea"><br>
          <form>
            <div>
              <div class="tabs">
                <div class="tab-item active" >
                  BÊN BỜ BIỂN
                </div>
                <div class="tab-item " >
                  DU THUYỀN TRÊN BIỂN
                </div>
                <div class="tab-line"></div>
              </div>

              <div class="tab-content">
                  <div class="tab-pane active">
                    <ul>
                      <li>
                        <label>Khách Sạn</label><br><br>
                        <input type="text" id="hotel" placeholder="Khách sạn ...">
                      </li>
                      <li>
                        <label>Địa Điểm</label><br><br>
                        <select id="place">
                        <?php
                          $sql = "SELECT *FROM tbl_bb ";

                          $res = mysqli_query($conn, $sql);
    
                          $count = mysqli_num_rows($res);
    
                          if($count>0)
                          {
                            while($row =mysqli_fetch_assoc($res))
                            {
                              
                              $matour = $row['matour'];
                          $madm = $row['madm'];
                          $tentour = $row['tentour'];
                          $thongtin = $row['thongtin'];
                          $thoigian = $row['thoigian'];
                          $image_name = $row['image'];
                          $image1 = $row['image1'];
                          $image2 = $row['image2'];
                          $image3 = $row['image3'];
                          $gia = $row['gia'];
                              ?>
                            <option value=""><?php echo $tentour;?></option>
                              <?php
                            }
                          }
                          else
                          {
                            echo "";
                          }
                        
                        ?>
                          
                          
                        </select>
                      </li>
                      <li>
                        <label>Ngày bắt đầu - kết thúc</label><br><br>
                        <input type="text" id="date" value="1/1/2021 - 1/1/2022">
                      </li>
                      <li>
                        <label>Số Lượng Thành Viên</label><br><br>
                        <input type="Text" id="member" value="4 người lớn , 0 trẻ em">
                      </li>
                      <li>
                        <label></label><br><br>
                        <a href="<?php echo SITEURL; ?>thongtintour.php?matour=<?php echo $matour; ?>">
                          <input type="button" id="btn" value="Đặt lịch">
                        </a>
                        
                      </li>
                    </ul> 

                  </div>

                  <div class="tab-pane">
                    <ul>
                      <li>
                        <label>Du Thuyền</label><br><br>
                        <input type="text" id="hotel" placeholder="Du thuyền...">
                      </li>
                      <li>
                        <label>Địa Điểm</label><br><br>
                        <select id="place">
                        <?php
                          $sql = "SELECT *FROM tbl_dt ";

                          $res = mysqli_query($conn, $sql);
    
                          $count = mysqli_num_rows($res);
    
                          if($count>0)
                          {
                            while($row =mysqli_fetch_assoc($res))
                            {
                              
                              $matour = $row['matour'];
                          $madm = $row['madm'];
                          $tentour = $row['tentour'];
                          $thongtin = $row['thongtin'];
                          $thoigian = $row['thoigian'];
                          $image_name = $row['image'];
                          $image1 = $row['image1'];
                          $image2 = $row['image2'];
                          $image3 = $row['image3'];
                          $gia = $row['gia'];
                              ?>
                            <option value=""><?php echo $tentour;?></option>
                              <?php
                            }
                          }
                          else
                          {
                            echo "";
                          }
                        
                        ?>
                          
                          
                        </select>
                      </li>
                      <li>
                        <label>Ngày bắt đầu - kết thúc</label><br><br>
                        <input type="text" id="date" value="1/1/2021 - 1/1/2022">
                      </li>
                      <li class="tab-member">
                        <label>Số Lượng Thành Viên</label><br><br>
                        <input type="Text" id="member" value="4 người lớn , 0 trẻ em">
                      </li>
                      <li>
                        <label></label><br><br>
                        <a href="<?php echo SITEURL; ?>thongtintour.php?matour=<?php echo $matour; ?>">
                          <input type="button" id="btn" value="Đặt lịch">
                        </a>
                      </li>
                    </ul> 
                  </div>
                  <div class="tab-line"></div>
              </div>
            </div> <script src="./tour.js"></script>
          </form>
        </div>

        <div class="rest"><br>
          <h1>ĐẶT KÌ NGHỈ CỦA BẠN VỚI MINHTUS</h1>
          <hr align="center" width="100px"><br>
          <p>
            Bạn đang có kế hoạch khám phá một thành phố với bạn bè của bạn? Hay đúng hơn là một kỳ nghỉ ở bãi biển với gia đình? Một hành trình qua các cảnh quan nhiệt đới? Những kỳ nghỉ với Mnhtus - và cuộc sống hàng ngày trôi đi. Đặt trực tuyến thoải mái mà không bị chậm trễ hơn nữa.
          </p>
          <hr align="center" width="200px">
          <br>
              <div>
                <a href="#">
                  <h2>CÔNG TY DU LỊCH</h2>
                <p>Tìm đại lí du lịch của bạn </p>
                </a>
              </div>
              <div>
                <a href="#">
                  <h2>CÔNG TY DU LỊCH</h2>
                <p>Tìm đại lí du lịch của bạn </p>
                </a>
              </div>
              <div>
                <a href="#">
                  <h2>CÔNG TY DU LỊCH</h2>
                <p>Tìm đại lí du lịch của bạn </p>
                </a>
              </div>
        </div>

        
        <div class="rest2"><br>
          <h1>ĐẶT KÌ NGHỈ CỦA BẠN VỚI MINHTUS</h1>
          <hr align="center" width="100px"><br>
          <p id="content-rest">
            Một trăm năm kinh nghiệm trong ngành du lịch và sự nhạy bén với các xu hướng mới khiến chúng tôi trở thành chuyên gia khi đề cập đến mong muốn và nhu cầu của bạn, đồng thời là nhà tiên phong trong các hình thức du lịch mới. Đây là truyền thống Minhtus.
          </p>
          <hr align="center" width="200px">
          <br>
                  <a href="#">
                    <div>
                    <img src="img/option1.jpg" align="center">
                    <p>Các kỳ nghỉ du lịch biển</p>
                  </div>
                  </a>
                  <a href="#">
                    <div>
                    <img src="img/option2.jpg" align="center">
                    <p>Các kỳ nghỉ du lịch đất liền</p>
                  </div>
                  </a>
                  <a href="#">
                    <div>
                    <img src="img/option3.jpg" align="center">
                    <p>Các kỳ nghỉ du lịch trên du thuyền</p>
                  </div>
                  </a>
                  <a href="#">
                    <div>
                    <img src="img/option4.jpg" align="center">
                    <p>Tour du lịch cả nước</p>
                  </div>
                  </a><br><br><br><br><br><br><br><br><br>
                  <a href="#" class="view-all"><p>Tất cả loại chuyến đi</p></a>
        </div>

        <div class="rest3"><br>
          <h1>CÁC ĐIỂM ĐẾN NỔI TIẾNG CHO CÁC NGÀY LỄ NĂM 2022</h1>
          <hr align="center" width="100">
          <p>Dù điểm đến mơ ước của bạn là gì, với Minhtus, nó nằm ngay trong tầm tay bạn. Để giúp bạn dễ dàng hơn trong việc tìm kiếm một kỳ nghỉ mà mọi thứ đều bị lãng quên, tại đây bạn sẽ tìm thấy những điểm đến đẹp nhất để đặt phòng sớm:</p>
          <hr align="center" width="200">
            <div class="cards">
            <?php
                      $sql1 = "SELECT *FROM tbl_bb LIMIT 8";

                      $res1 = mysqli_query($conn, $sql1);

                      $count1 = mysqli_num_rows($res1);

                      if($count1>0)
                      {
                        while($rows1 =mysqli_fetch_assoc($res1))
                        {
                          $matour = $rows1['matour'];
                          $madm = $rows1['madm'];
                          $tentour = $rows1['tentour'];
                          $thongtin = $rows1['thongtin'];
                          $thoigian = $rows1['thoigian'];
                          $image_name = $rows1['image'];
                          $image1 = $rows1['image1'];
                          $image2 = $rows1['image2'];
                          $image3 = $rows1['image3'];
                          $gia = $rows1['gia'];
                          ?>

                    <div class="card-item">
                      <a href="<?php echo SITEURL; ?>thongtintour.php?matour=<?php echo $matour; ?>">
                        <div >
                          <img src="<?php echo SITEURL; ?>images/<?php echo $image_name; ?>">
                          
                        </div>
                        </a>
                        <div class="card-content">
                          <h4><i class="fas fa-map-marker-alt"></i> <a href="<?php echo SITEURL; ?>thongtintour.php?matour=<?php echo $matour; ?>"><?php echo $tentour;?></a></h4>
                          <p id="tt"><?php echo $thongtin; ?></p>
                          <span>Giá từ : <?php echo $gia;?> VNĐ</span><br><br>
                          <button id="btnTour" onclick="setTour()"><a href="<?php echo SITEURL; ?>giohang.php?matour=<?php echo $matour; ?>">Đặt lịch</a></button>
                      </div>
                    </div>

                            
                          <?php
                        }
                      }
                      else
                      {
                        echo "";
                      }
                    
                    ?>
            </div>
        </div>

        <div class="history">
           <br>
            <h1>NHỮNG ĐỊA ĐIỂM NỔI TIẾNG ĐƯỢC ĐÁNH GIÁ CAO</h1>
            <p>Câu chuyện du lịch từ khắp nơi trên thế giới và những nơi mà bạn quên đi mọi thứ.</p>
            <div>
              <div class="card-history">
              <?php
                      $sql = "SELECT *FROM tbl_dt LIMIT 10";

                      $res = mysqli_query($conn, $sql);

                      $count = mysqli_num_rows($res);

                      if($count>0)
                      {
                        while($row =mysqli_fetch_assoc($res))
                        {
                          $matour = $row['matour'];
                          $madm = $row['madm'];
                          $tentour = $row['tentour'];
                          $thongtin = $row['thongtin'];
                          $thoigian = $row['thoigian'];
                          $image_name = $row['image'];
                          $image1 = $row['image1'];
                          $image2 = $row['image2'];
                          $image3 = $row['image3'];
                          $gia = $row['gia'];
                          ?>

                  <div class="card-history-content">
                    <a href="<?php echo SITEURL; ?>thongtintour.php?matour=<?php echo $matour; ?>"><img src="<?php echo SITEURL;?>images/<?php echo $image2;?>">
                      <div class="history-content"><i class="fa fa-dot-circle"></i><?php echo $tentour;?></div>
                      <div class="history-down">Điểm đến<br> Qua lại <?php echo $tentour;?></div>
                    </a>
                  </div>
                          <?php
                        }
                      }
                      else
                      {
                        echo "";
                      }
                    
                    ?>
              </div>
            </div>
        </div>

        <div class="come-on">
          <h1>KỲ NGHỈ CỦA BẠN ĐANG Ở TRONG TẦM TAY TỐT NHẤT VỚI CHÚNG TÔI. </h1>
          <hr align="center" width="100">
          <div>
            <div class="show-list">
              <p><i class="fa fa-check"></i>Hơn 50 năm kinh nghiệm dụ lịch</p>
              <p><i class="fa fa-check"></i>Lời khuyên từ chuyên gia du lịch của chúng tôi</p>
              <p><i class="fa fa-check"></i>Lời khuyên từ chuyên gia du lịch của chúng tôi</p>
            </div>
            <div class="show-list">
              <p><i class="fa fa-check"></i>Hơn 50 năm kinh nghiệm dụ lịch</p>
              <p><i class="fa fa-check"></i>Lời khuyên từ chuyên gia du lịch của chúng tôi</p>
              <p><i class="fa fa-check"></i>Lời khuyên từ chuyên gia du lịch của chúng tôi</p>
            </div>
          </div>
          <br><br><br><br><br><br>
          <a href="#"><p>Tìm hiểu thêm</p></a>
        </div> 
      </div>

      <?php require './footer.php';?>