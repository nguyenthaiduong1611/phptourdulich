<?php require './admin/config/constants.php'; ?> 
<div class="header">
        <div class="up">
          <div class="logo" >
              <a href="http://127.0.0.1:5500/trangchu.html#"><img src="./img/logo.jpg" id="logo" width="250" height="110"></a>
          </div>
          <ul>
              <a href="#"><li class="img_item"><i class="fab fa-facebook-square" aria-hidden="true" style="font-size: 15px;"></i></li></a>
              <a href="#"><li class="img_item"><i class="fa fa-envelope" aria-hidden="true" style="font-size: 15px;"></i></li></a>
              <a href="#"><li class="img_item"><i class="fab fa-youtube" aria-hidden="true" style="font-size: 15px;"></i></li></a>
              <a href="#"><li class="img_item"><i class="fa fa-phone-square" aria-hidden="true" style="font-size: 15px;">0334636815</i></li></a>
          </ul>
          <div class="search-box">
            <input type="search" name="search" class="search-txt" placeholder="Tìm kiếm">
            <a href="#" class="search-btn" name="search-btn">
                <i class="fa fa-search"></i>
            </a>
          </div>  
          <div class="user">
              <a href="http://localhost:80/tour-order/loginkh.php#"><i class="fas fa-users"></i></a>
            </div>
      </div>

        <div class="header-dow">
              <div class="menu">
                
                <div class="dropdown">
                    <p class="dropbtn"><a href="http://localhost:80/tour-order/trangchu.php#">Trang chủ</a></p>
                  </div>
                  
                  <div class="dropdown">
                    <p class="dropbtn"><a href="#">Du lịch đất liền</a></p>
                    
                    <div>
                    <div class="dropdown-content active">
                      <?php
                      $sql = "SELECT *FROM tbl_danhmuc ";

                      $res = mysqli_query($conn, $sql);

                      $count = mysqli_num_rows($res);

                      if($count>0)
                      {
                        while($row =mysqli_fetch_assoc($res))
                        {
                          $STT= $row['STT'];
                          $title = $row['title'];
                          ?>
                            <a href="#" class="dropdown-item active"><?php echo $title; ?></a>
                          <?php
                        }
                      }
                      else
                      {
                        echo "";
                      }
                      $sql = "SELECT *FROM tbl_travel ";

                      $res = mysqli_query($conn, $sql);

                      $count = mysqli_num_rows($res);

                      if($count>0)
                      {
                        while($row =mysqli_fetch_assoc($res))
                        {
                          
                                    $madm = $row['madm'];
                                    $tentour = $row['tentour'];
                                    $thongtin = $row['thongtin'];
                                    $gia = $row['gia'];
                                    $matour = $row['matour'];
                                    $thoigian = $row['thoigian'];
                                    $image_name = $row['image'];
                                    $image1 = $row['image1'];
                                    $image2 = $row['image2'];
                                    $image3 = $row['image3'];
                          ?>
                    <div class="travel">
                      <div class="tab-dropdown-content active">
                        <div class="dropdown-content-travel">
                          <a href="<?php echo SITEURL; ?>thongtintour.php?matour=<?php echo $matour; ?>">Du lịch <?php echo $tentour; ?></a>
                          
                        </div>
                      </div>
                    </div>  
                          <?php
                        }
                      }
                      else
                      {
                        echo "";
                      }
                    
                    ?>
                    
                    </div>
                  </div>
                </div>
                  <div class="dropdown">
                    <p class="dropbtn"><a href="<?php echo SITEURL;?>dlbien.php">Du lịch biển</a></p>
                    
                  </div>
                  <div class="dropdown">
                    <p class="dropbtn"><a href="#">Vé máy bay</a></p>
                    
                  </div>
                  <div class="dropdown">
                    <p class="dropbtn"><a href="#">Khuyến mãi</a></p>
                    <div class="dropdown-content-event">
                    <?php
                      $sql = "SELECT *FROM tbl_sukien ";

                      $res = mysqli_query($conn, $sql);

                      $count = mysqli_num_rows($res);

                      if($count>0)
                      {
                        while($row =mysqli_fetch_assoc($res))
                        {
                          $mask= $row['mask'];
                          $tensk = $row['tensk'];
                          $noidung = $row['noidung'];
                          $matour = $row['matour'];
                          ?>
                            <a >Mã khuyến mãi : <?php echo $mask;?><br>
                            Tên sự kiện :<?php echo $tensk; ?> 
                          <?php
                        }
                      }
                      else
                      {
                        echo "";
                      }
                    
                    ?>
                    </div>
                  </div>
                  
                  <div class="dropdown">
                    <p class="dropbtn"><a href="#">Giới thiệu</a></p>
                  </div>
            </div>   
            
          </div> 
        </div>
      </div>