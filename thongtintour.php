
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./thongtintour.css">
    <title>Du lịch vịnh Lan Hạ</title>
</head>
<body>

        <?php require './menu.php'; ?>

        <div class="infor">
        <?php
          $matour = $_GET['matour'];

          $sql = "SELECT * FROM tbl_travel WHERE matour=$matour";
        
          $res = mysqli_query($conn, $sql);

          if($res==TRUE)
          {
              $count = mysqli_num_rows($res);

              if($count==1)
              {
                  $row = mysqli_fetch_assoc($res);

                  $madm = $row['madm'];
                  $tentour = $row['tentour'];
                  $thongtin = $row['thongtin'];
                  $gia = $row['gia'];
                  $matour = $row['matour'];
                  $thoigian = $row['thoigian'];
                  $image_name = $row['image'];
                  $image1 = $row['image1'];
                  $image2 = $row['image2'];
                  $image3 = $row['image3'];
              }
              
              else
              {
                  die();
              }
          }
        ?>
          <div class="img-item">
            <img class="slide" src="<?php echo SITEURL; ?>images/<?php echo $image_name; ?>" alt="" idx="0" width="400" height="450">
            <img class="slide" src="<?php echo SITEURL; ?>images/<?php echo $image1; ?>" alt="" idx="1" width="400" height="450">
            <img class="slide" src="<?php echo SITEURL; ?>images/<?php echo $image2; ?>" alt="" idx="2" width="400" height="450">
            <img class="slide" src="<?php echo SITEURL; ?>images/<?php echo $image3; ?>" alt="" idx="3" width="400" height="450">
              <div class="change-img">
                <button class="active" idx="0"><img src="<?php echo SITEURL; ?>images/<?php echo $image_name; ?>" width="80" height="80"></button>
                <button idx="1"><img src="<?php echo SITEURL; ?>images/<?php echo $image1; ?>" alt="" width="80" height="80"></button>
                <button idx="2"><img src="<?php echo SITEURL; ?>images/<?php echo $image2; ?>" alt="" width="80" height="80"></button>
                <button idx="3"><img src="<?php echo SITEURL; ?>images/<?php echo $image3; ?>" alt="" width="80" height="80"></button>
              </div>
            </div>      
        
            <div class="buy">
              <div>
                <h3><i class="fas fa-map-marker-alt"></i> <?php echo $tentour;?></h3>
              </div>
              <div>
                <p><i class="fa fa-history"></i> Thời gian : <?php echo $thoigian?></p>
              </div>
              <div>
                <p><i class="fa fa-dollar-sign"></i> Giá : <?php echo $gia;?> vnđ</p>
              </div>
              <div class="tt">
                <p><i class="far fa-question-circle"></i> Thông tin : <?php echo $thongtin;?></p>
              </div>
              <div>
                <a href="<?php echo SITEURL; ?>thanhtoan.php?matour=<?php echo $matour; ?>">
                  <input type="submit" id="submit" value="Đặt lịch">
                </a>
                
              </div>
            </div> 
            <script src="./script.js"></script>
        </div>
        <hr width="100%">

        <div class="content">
          <div>
            <h1>Giới thiệu chung về <?php echo $tentour;?></h1>
            <p>Tọa lạc tại phía nam vịnh Hạ Long và phía đông của đảo Cát Bà, vịnh Lan Hạ đang trở thành một điểm du lịch được nhiều du khách yêu thích trong thời gian gần đây. Quần đảo này rộng hơn 7000 ha, sở hữu trên 400 hòn đảo lớn nhỏ khác nhau, với những bãi tắm chưa bị khai thác nhiều. Điểm hấp dẫn của vịnh Lan Hạ chính là vẻ đẹp hoang sơ, không gian yên tĩnh không đâu có được, rất phù hợp là điểm đến cho những ai muốn tìm kiếm không gian mới để nghỉ dưỡng. Bên cạnh đó, du lịch vịnh Lan Hạ, bạn còn có có cơ hội được ghé thăm làng chài Cửa Vạn, làng chài Cái Bèo để khám phá cuộc sống thường nhật của những người ngư dân nơi đây.</p>
            <img src="<?php echo SITEURL;?>images/<?php echo $image_name;?>">
            <h1>Thời tiết tại <?php echo $tentour;?></h1>
            <p>Khoảng thời gian đi du lịch vịnh Lan Hạ hợp lý nhất là từ tháng 4 đến tháng 7, thời tiết đẹp thích hợp cho các hoạt động bơi lội và vui chơi giải trí trên Vịnh Lan Hạ. Kinh nghiệm du lịch vịnh Lan Hạ, các bạn nên tránh đi du lịch tại đây vào khoảng thời gian cuối hè (tháng 8 - 10), vì lúc này có nhiều mưa và bão, thời tiết không thuận tiện cho chuyến đi.</p>
            <img src="<?php echo SITEURL;?>images/<?php echo $image1;?>">
            <h1>Ở đâu khi đến <?php echo $tentour;?></h1>
            <h3>Các khách sạn trên đảo Cát Bà</h3>
            <p>Vịnh Lan Hạ nằm ở ngoài đảo, nên hệ thống khách sạn cũng khá ít ỏi, việc tìm nơi cư trú gần vịnh rất khó. Bạn có thể tham khảo một số nhà nghỉ, khách sạn trên vịnh Lan Hạ có giá rẻ, view đẹp nhất dưới đây:

              - Cat Ba Lucky Family Hotel: Là lựa chọn hoàn hảo cho kì nghỉ 1 đêm của bạn. Với đầy đủ tiện nghi, nhân viên phục vụ tận tình, view đẹp,...khách sạn này được nhiều du khách đánh giá rất tốt. Nếu còn phân vân không biết ở đâu khi đến vịnh Lan Hạ thì bạn có thể tham khảo khách sạn này nhé!
              
              Địa chỉ: 25 Núi Ngọc, Cát Bà.
              
              Giá phòng tham khảo thấp nhất khoảng 450.000đ/ đêm.
              
              - Little Cat Ba: Khách sạn này nằm gần vịnh Lan Hạ và được trang bị đầy đủ tiện nghi đáp ứng được nhu cầu của du khách. Ngoài ra, khách sạn còn cung cấp thêm nhiều dịch vụ khác như bãi tắm riêng, khu vui chơi, thể thao,...hứa hẹn là nơi giúp bạn có những trải nghiệm trọn vẹn nhất cho chuyến du lịch vịnh Lan Hạ của mình. Little Cat Ba có nhiều hạng phòng cho bạn lựa chọn như: phòng loại 1, phòng loại 2, phòng dorm. Tùy vào nhu cầu, bạn có thể lựa chọn loại phòng phù hợp với mình nhất.
              
              Địa chỉ: 350 đường Hà Sen, Vịnh Lan Hạ.
              
              Giá phòng tham khảo thấp nhất khoảng 700.000đ/ đêm.
            </p>
            <img src="<?php echo SITEURL;?>images/<?php echo $image2;?>" width="600" height="450">
            <h3>Du thuyền Cát bà</h3>
            <p>Ngoài ra, bạn có thể lựa chọn một hình thức mới để tham quan vịnh Lan Hạ, đó là lựa chọn du thuyền. Đặt tour du thuyền Hạ Long có tuyến di chuyển qua Vịnh Lan Hạ, bạn sẽ có một trải nghiệm hoàn toàn mới lạ khi được ăn, ngủ, chơi, tham gia nhiều hoạt động thú vị ngay trên du thuyền. Đặc biệt, có đủ các loại du thuyền với chất lượng dịch vụ đẳng cấp từ 3-5* để bạn lựa chọn, giá cả khoảng hơn 2.000.000đ/ khách. Hiện nay, có một số du thuyền Hạ Long chạy qua vịnh Lan Hạ như: Orchid, Mon Cheri, Era, Azalea, La Regina,...Đây là những du thuyền 5* nổi bật và rất được yêu thích nhất trong năm nay. </p>
            <h1>Đi chơi gì tại <?php echo $tentour;?></h1>
            <h2>Những địa điểm không thể bỏ qua tại <?php echo $tentour;?></h2>
            <p>Vịnh Lan Hạ có khá nhiều điểm du lịch, dưới đây là những địa điểm nổi tiếng nhất tại vịnh Lan Hạ mà bạn không nên bỏ qua.</p>
            <h3>Làng chài Cửa Vạn</h3>
            <p>Làng chài Cửa Vạn là một kỳ quan độc đáo của những ngư dân. Đây là điểm dừng mà các tàu đưa khách đi thăm vịnh Lan Hạ trong ngày thường ghé qua. Làng chài nằm cách bến Bèo khoảng 12km (1 tiếng rưỡi đi tàu). Tại đây bạn có thể được trực tiếp tham gia chèo thuyền, giăng lưới, thả câu bắt tôm cá cùng các thôn nữ trong làng.</p>
            <h3>Đảo Nam Cát Bà</h3>
            <p>Để đến điểm du lịch này, bạn di chuyển từ bến Bèo theo hướng ra vịnh Lan Hạ khoảng 15 phút sẽ tới đảo Nam Cát. Hòn đảo này sẽ là một lựa chọn tốt cho du khách, đặc biệt với những người yêu thiên nhiên. Bãi biển Nam Cát với các bãi tắm hoang sơ sẽ là nơi bạn có thể thả mình dưới ánh nắng, ngâm mình trong làn nước trong xanh mát mẻ. Không giống như những bãi biển khác, nơi đây có những bãi cát nhỏ tạo không gian riêng tư dành riêng cho bạn</p>
            <h3>Đảo Khỉ</h3>
            <p>Để đến đảo khỉ khách du lịch thường đi thuyền từ bến Bèo mất khoảng 10 phút đi qua làng chài Cái Bèo, qua mấy hòn đảo nhỏ rồi tiến thẳng ra đến đảo khỉ. Đảo khỉ là điểm đến lý  tưởng cho chuyến du lịch vịnh Lan Hạ của bạn vì nước biển ở đây trong và xanh vô cùng, cùng với đó là hai bãi tắm Cát Dứa 1 và Cát Dứa 2 rất xinh đẹp. Bạn cũng có thể khám phá nơi sinh sống của những chú khỉ nơi đây, chắc hẳn sẽ mang lại nhiều trải nghiệm đáng nhớ.</p>
            <h3>Hang Luồn</h3>
            <p>Với khung cảnh thiên nhiên hữu tình, khi tới hang Luồn bạn sẽ được chèo thuyền kayak chui qua hang và ngắm nhìn cảnh vật xinh đẹp xung quanh. Bạn cũng có thăm hang Luồn bằng cách thuê tàu đi du lịch trong ngày trên vịnh. Kinh nghiệm du lịch vịnh Lan Hạ, bạn nên thử chèo thuyền kayak một lần - một trong những hoạt động thú vị không thể bỏ qua khi tới đây.</p>
            <img src="<?php echo SITEURL;?>images/<?php echo $image3;?>" width="600" height="450">
            <h3>Bãi tắm Vạn Bội</h3>
            <p>Bãi tắm Vạn Bội nằm trên một hòn đảo nhỏ đối diện với đảo Rùa Giống trong vịnh Lan Hạ. Bạn có thể thuê những chiếc thuyền nan, thuyền kayak, di chuyển qua các đảo nhỏ của vịnh để đến với bãi tắm này. Đến Lan Hạ rồi thì đừng bỏ qua bãi tắm Vạn Bội nhé, bởi nơi đây còn khá hoang sơ và vẫn giữ được những nét đẹp tự nhiên vốn có. Đây chính là nơi bạn có thể tận hưởng không gian yên tĩnh, thư thái mà ít bãi tắm nào có được.</p>
            <h3>Những lưu ý khi đi du lịch vịnh Lan Hạ</h3>
            <p>Để chuyến du lịch của bạn trở nên thuận tiện hơn, dưới đây là một số điều cần lưu ý khi đi du lịch vịnh Lan Hạ:
              <br>
              - Buổi đêm ở Vịnh Lan Hạ, thời tiết thường khá lạnh, do đó, các bạn nên chuẩn bị trước một chiếc áo ấm bên mình.
              <br>
              - Hãy cẩn thận với những lời giới thiệu, lời mời đi câu mực đêm nếu như bạn không chắc chắn, thông thường mức giá sẽ là 500.000đ/ 6 người để đi câu hải sản và chiến lợi phẩm có thể được mang về chế biến hoặc ăn sống trực tiếp bằng cách vắt chanh hoặc chấm với mù tạt.
              <br>
              - Hầu hết các dịch vụ ở vịnh Lan Hạ, Cát Bà đều có bên trung gian nên các bạn nhớ tìm hiểu và cân nhắc kỹ lưỡng trước khi thuê hay mua bất cứ thứ gì nhé.
              <br>
              - Đặt du thuyền Cát Bà trước ít nhất 3 -7 ngày để đảm bảo còn phòng</p>
          </div>
          
        </div>

          <div class="footer">
            <div>
                <div>
                  <a href="#"><h4>TÌM KIẾM VÀ ĐẶT CHỖ</h4></a>
                  <hr align="left" width="50">
                  <a href="#"><p>CÁC KÌ NGHỈ BÊN BỜ BIỂN</p></a>
                  <a href="#"><p>NGÀY CUỐI TUẦN</p></a>
                  <a href="#"><p>DU LỊCH TRÊN BIỂN</p></a>
                  
                </div>
                <div>
                  <a href="#"><h4>TÌM KIẾM VÀ ĐẶT CHỖ</h4></a>
                  <hr align="left" width="50">
                  <a href="#"><p>CÁC KÌ NGHỈ BÊN BỜ BIỂN</p></a>
                  <a href="#"><p>NGÀY CUỐI TUẦN</p></a>
                  <a href="#"><p>DU LỊCH TRÊN BIỂN</p></a>
                  
                </div>
                <div>
                  <a href="#"><h4>TÌM KIẾM VÀ ĐẶT CHỖ</h4></a>
                  <hr align="left" width="50">
                  <a href="#"><p>CÁC KÌ NGHỈ BÊN BỜ BIỂN</p></a>
                  <a href="#"><p>NGÀY CUỐI TUẦN</p></a>
                  <a href="#"><p>DU LỊCH TRÊN BIỂN</p></a>
                  
                </div>
                <div>
                  <a href="#"><h4>TÌM KIẾM VÀ ĐẶT CHỖ</h4></a>
                  <hr align="left" width="50">
                  <a href="#"><p>CÁC KÌ NGHỈ BÊN BỜ BIỂN</p></a>
                  <a href="#"><p>NGÀY CUỐI TUẦN</p></a>
                  <a href="#"><p>DU LỊCH TRÊN BIỂN</p></a>
                  
                </div>
              </div>
              <hr width="100%" color="white">
     
                <div class="follow">
                  <h4>THEO DÕI CHÚNG TÔI</h4>
                  <a href="#"><i class="fab fa-facebook-f"></i></a>
                  <a href="#"><i class="fab fa-youtube"></i></a>
                  <a href="#"><i class="fab fa-instagram"></i></a>
                </div>
                <video width="400" height="400" autoplay loop>
                  <source src="img/minhtus.mp4">
                </video>
            </div>
          </div>
          <div class="copy-right">
            <a href="#"><i class=" fa fa-copyright"></i>2021 by Minhtus</a>
          </div>
</body>
</html>