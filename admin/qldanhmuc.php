<?php require 'menu.php'; ?>
<?php require './config/constants.php'; ?>

          <div id="content">
            <nav class="navbar navbar-default">
              <div class="container-fluid active">
                  <div class="header">
                  <div class="manage-admin">
                      <div>
                          <div style="margin-left:150px; margin-top: 100px;">
                              <h3>Quản Lí Danh Mục</h3>
                            </div>
                      </div>
                      <div>
                          <button id="btn" onclick="myAdd()"><a href="http://localhost:80/tour-order/admin/add-danhmuc.php">Thêm Danh Mục</a></button>
                      </div>
                      <table border="1">
                          <tr>
                              <th >STT</th>
                              <th >Mã danh mục</th>
                              <th >Title</th>
                              <th>Quản lý</th>
                          </tr>
                        
                          <?php
                            
                            $sql = "SELECT * FROM tbl_danhmuc";
                            $res = mysqli_query($conn, $sql);
                            if($res==TRUE) 
                            {
                                $count = mysqli_num_rows($res);
                                $stt=1;
                                if($count>0) 
                                {
                                    while($rows=mysqli_fetch_assoc($res))
                                    {
                                        $STT = $rows['STT'];
                                        $madm = $rows['madm'];
                                        $title = $rows['title'];
                                        ?>

                                         <tr>
                                            <td><?php echo $stt++; ?></td>
                                            <td><?php echo $madm; ?></td>
                                            <td><?php echo $title; ?></td>
                                            <td>
                                                <button id="btnUpdate" onclick="myUpdate()"><a href="<?php echo SITEURL; ?>admin/update-danhmuc.php?STT=<?php echo $STT; ?>">Sửa</a></button>
                                                <button id="btnDelete" onclick="myDelete()"><a href="<?php echo SITEURL; ?>admin/delete-danhmuc.php?STT=<?php echo $STT; ?>">Xóa</a></button>
                                            </td>
                                        </tr>                                           
                                        
                                        <?php
                                    }
                                }
                                else
                                {

                                }
                            }
                          
                          ?>
                      </table>
                  </div>
              </div>
            </nav>
          </div> 
<?php require 'footer.php'; ?>