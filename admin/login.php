
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./dangnhap.css">
    <title>Admin</title>
    </head>
    <body>
        <div class="login-box">
            <h2>Đăng nhập</h2>
            <form action="login-ad.php" method="POST" >
              <div class="user-box">
                <input type="text" id="username" name="username" required="">
                <label>Tài khoản</label>
              </div>
              <div class="user-box">
                <input type="password" id="password" name="password" required="">
                <label>Mật khẩu</label>
              </div>
              <a href="#">
                <span></span>
                 <button id="dangnhap" onclick="myBtn()">Đăng Nhập</button>
              </a>
            </form>
            <script src="./login.js"></script>

          </div>
    </body>

</html>