-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th12 17, 2021 lúc 09:04 AM
-- Phiên bản máy phục vụ: 10.4.18-MariaDB
-- Phiên bản PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `tour-order`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `STT` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `matkhau` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbl_admin`
--

INSERT INTO `tbl_admin` (`STT`, `fullname`, `username`, `matkhau`) VALUES
(7, 'Nguyễn Thái Dương1234', 'admin01235', '123456'),
(8, 'Nguyễn Minh Quân', 'admin1', '123456'),
(9, 'Trần Công Minh', 'admin2', '123456');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_bb`
--

CREATE TABLE `tbl_bb` (
  `matour` int(11) NOT NULL,
  `madm` varchar(50) NOT NULL,
  `tentour` varchar(255) NOT NULL,
  `thongtin` varchar(255) NOT NULL,
  `thoigian` varchar(255) NOT NULL,
  `gia` float NOT NULL,
  `image` varchar(255) NOT NULL,
  `image1` varchar(255) NOT NULL,
  `image2` varchar(255) NOT NULL,
  `image3` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbl_bb`
--

INSERT INTO `tbl_bb` (`matour`, `madm`, `tentour`, `thongtin`, `thoigian`, `gia`, `image`, `image1`, `image2`, `image3`) VALUES
(13124, '222', 'Ha Long', 'sadasdasd', '2 ngay 1 dem', 1234560, 'tour-name8145.jpg', 'tour-name5344.jpg', 'tour-name5763.jpg', 'tour-name9780.jpg'),
(1321431, '30092021', 'Sầm Sơn', '', '2 ngay 1 dem', 5200000, 'tour-name2147.jpg', 'tour-name8246.jpg', 'tour-name2073.jpg', 'tour-name815.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_danhmuc`
--

CREATE TABLE `tbl_danhmuc` (
  `STT` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `madm` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbl_danhmuc`
--

INSERT INTO `tbl_danhmuc` (`STT`, `title`, `madm`) VALUES
(11, 'Địa điểm du lịch trên cả nước', 'ALL');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_dt`
--

CREATE TABLE `tbl_dt` (
  `matour` int(11) NOT NULL,
  `madm` varchar(50) NOT NULL,
  `tentour` varchar(255) NOT NULL,
  `thongtin` varchar(255) NOT NULL,
  `thoigian` varchar(255) NOT NULL,
  `gia` float NOT NULL,
  `image` varchar(255) NOT NULL,
  `image1` varchar(255) NOT NULL,
  `image2` varchar(255) NOT NULL,
  `image3` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_hoadon`
--

CREATE TABLE `tbl_hoadon` (
  `fullname` varchar(255) NOT NULL,
  `SDT` int(11) NOT NULL,
  `tentour` varchar(255) NOT NULL,
  `thoigian` varchar(255) NOT NULL,
  `gia` float NOT NULL,
  `matour` int(11) NOT NULL,
  `CCCD` int(15) NOT NULL,
  `people` int(11) NOT NULL,
  `ngaykhuhoi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbl_hoadon`
--

INSERT INTO `tbl_hoadon` (`fullname`, `SDT`, `tentour`, `thoigian`, `gia`, `matour`, `CCCD`, `people`, `ngaykhuhoi`) VALUES
('Nguyễn Thái Dương', 123456, 'Long An', '', 7990000, 8824342, 123213, 2, '25/03/2022-28/03/2022'),
('Lê Quang Tú', 123456789, 'Huế', '', 5000000, 227092021, 23332212, 3, '12/11/2021-15/11/2021'),
('Trần Xuân Phong', 334636815, 'Lạng Sơn', '3 ngày / 2 đêm', 2499000, 13124, 323123, 5, '12/11/2021-15/11/2021'),
('Nguyen van an', 1288323232, 'Hà Nam', '2 ngay 1 dem', 2499000, 29092001, 7739212, 2, '25/03/2022-28/03/2022'),
('Nguyễn Thái Dương', 2147483647, 'Huế', '', 5000000, 227092021, 123213, 4, '25/03/2022-28/03/2022');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_kh`
--

CREATE TABLE `tbl_kh` (
  `STT` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `SDT` int(15) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbl_kh`
--

INSERT INTO `tbl_kh` (`STT`, `fullname`, `username`, `password`, `SDT`, `email`) VALUES
(4, 'Nguyễn Đức Minh', 'ndm6301', '', 334636815, 'ducminhminh2001@gmail.com'),
(5, 'Lê Quang Tú', 'lqtu2901', 'lequangtu', 123456789, 'lquangtu@gmail.com'),
(6, 'Trần Xuân Phong', 'txp2001', 'txp2001', 123456782, 'txp2001@gmail.com');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_sukien`
--

CREATE TABLE `tbl_sukien` (
  `mask` int(11) NOT NULL,
  `matour` int(11) NOT NULL,
  `tensk` varchar(255) NOT NULL,
  `noidung` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbl_sukien`
--

INSERT INTO `tbl_sukien` (`mask`, `matour`, `tensk`, `noidung`) VALUES
(2021, 27092021, 'Chào hè 2021', ''),
(2022, 27092021, 'Endless Summer 2021', 'Giảm giá combo du lịch dành cho 2 người '),
(28092021, 2022, 'Mừng ngày phụ nữ Việt Nam', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_travel`
--

CREATE TABLE `tbl_travel` (
  `matour` int(11) NOT NULL,
  `madm` varchar(50) NOT NULL,
  `tentour` varchar(255) NOT NULL,
  `thongtin` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `gia` int(11) NOT NULL,
  `image1` varchar(255) NOT NULL,
  `image2` varchar(255) NOT NULL,
  `image3` varchar(255) NOT NULL,
  `thoigian` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbl_travel`
--

INSERT INTO `tbl_travel` (`matour`, `madm`, `tentour`, `thongtin`, `image`, `gia`, `image1`, `image2`, `image3`, `thoigian`) VALUES
(227092021, '2022', 'Huế', 'Du lịch Huế khám phá những nét cổ xưa ', 'tour-name4463.jpg', 5000000, 'tour-name6743.jpg', 'tour-name1399.jpg', 'tour-name8469.jpg', ''),
(327092021, '2023', 'An Giang', 'Các địa điểm du lịch miền Tây Nam Bộ không thể bỏ qua các địa điểm du lịch An Giang nổi tiếng như: rừng tràm Trà Sư, Núi Cấm,...', 'tour-name3725.jpg', 13000000, 'tour-name7307.jpg', 'tour-name707.jpg', 'tour-name1906.jpg', '1 tuần'),
(123124, '2021', 'Sapa', 'Sapa là “nơi gặp gỡ giữa trời và đất” với cảnh sắc thiên nhiên hùng vĩ bậc nhất miền Bắc. Đây cũng là nơi hội tụ đồng bào các dân tộc thiểu số sinh sống bao đời nay. Bạn đã đến để chiêm ngưỡng và trải nghiệm vùng đất đầy thú vị này chưa? ', 'tour-name9286.jpg', 4000000, 'tour-name1058.jpg', 'tour-name5148.jpg', 'tour-name2122.jpg', '3 ngày / 2 đêm'),
(4342, '2022', 'Cửa Lò', 'Du lịch Cửa Lò có bãi biển khá đẹp, không tắm thì tiếc cả đời.\r\n\r\nNguồn bài viết: https://dulichkhampha24.com/kinh-nghiem-du-lich-cua-lo-nghe-an.html', 'tour-name5240.jpg', 5000000, 'tour-name2593.jpeg', 'tour-name6849.jpg', 'tour-name6387.jpg', '2 ngay 1 dem'),
(8824342, '2023', 'Long An', 'Vùng đất miền Tây – Long An mang vẻ đẹp thanh bình của sông nước. Các địa điểm du lịch Long An là nơi bạn có thể cảm nhận những cánh  đồng sen ngát hương,', 'tour-name6941.jpg', 7990000, 'tour-name8836.jpg', 'tour-name9156.jpg', 'tour-name3206.jpg', ''),
(83845, '2024', 'Lâm Đồng -Long An', '', 'tour-name4512.jpg', 2399000, 'tour-name358.jpg', 'tour-name8217.jpg', 'tour-name2156.jpg', '1 tuần'),
(13124, '2021', 'Lạng Sơn', 'Lạng Sơn là vùng đất địa đầu của Tổ quốc với nhiều di tích lịch sử cùng phong cảnh thiên nhiên thơ mộng, hữu tình. ', 'tour-name1545.jpg', 2499000, 'tour-name77.jpg', 'tour-name430.jpg', 'tour-name9501.jpg', '3 ngày / 2 đêm'),
(42365, '2022', 'Sầm Sơn', 'Sầm Sơn Thanh Hoá đang là hành trình thu hút rất nhiều du khách trong và ngoài nước. Từ các khu du lịch sinh thái đến các du lịch tâm linh sẽ cho du khách khám phá một Thanh Hoá rất khác, quyến rũ hơn, lãng mạn hơn và sôi động hơn.', 'tour-name6739.jpg', 4499000, 'tour-name1526.jpg', 'tour-name2322.jpg', 'tour-name905.jpg', '3 ngày / 2 đêm'),
(1312, '2023', 'Cần thơ', '', 'tour-name9738.jpg', 7890000, 'tour-name6026.jpg', 'tour-name404.jpg', 'tour-name1209.jpg', ''),
(29092001, '92021', 'Hà Nam', '', 'tour-name8733.jpg', 2499000, 'tour-name4650.jpg', 'tour-name9357.jpg', 'tour-name2601.jpg', '2 ngay 1 dem'),
(123003, '30092021', 'Tây Nguyên', 'Về với Tây nguyên chốn du lịch hoang dã gần gũi với con người', 'tour-name4799.jpg', 0, 'tour-name4540.jpg', 'tour-name1957.jpg', 'tour-name5950.jpg', '');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`STT`);

--
-- Chỉ mục cho bảng `tbl_bb`
--
ALTER TABLE `tbl_bb`
  ADD PRIMARY KEY (`matour`);

--
-- Chỉ mục cho bảng `tbl_danhmuc`
--
ALTER TABLE `tbl_danhmuc`
  ADD PRIMARY KEY (`STT`);

--
-- Chỉ mục cho bảng `tbl_dt`
--
ALTER TABLE `tbl_dt`
  ADD PRIMARY KEY (`matour`);

--
-- Chỉ mục cho bảng `tbl_hoadon`
--
ALTER TABLE `tbl_hoadon`
  ADD PRIMARY KEY (`SDT`);

--
-- Chỉ mục cho bảng `tbl_kh`
--
ALTER TABLE `tbl_kh`
  ADD PRIMARY KEY (`STT`);

--
-- Chỉ mục cho bảng `tbl_sukien`
--
ALTER TABLE `tbl_sukien`
  ADD PRIMARY KEY (`mask`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `STT` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `tbl_bb`
--
ALTER TABLE `tbl_bb`
  MODIFY `matour` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1321432;

--
-- AUTO_INCREMENT cho bảng `tbl_danhmuc`
--
ALTER TABLE `tbl_danhmuc`
  MODIFY `STT` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `tbl_dt`
--
ALTER TABLE `tbl_dt`
  MODIFY `matour` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28092022;

--
-- AUTO_INCREMENT cho bảng `tbl_kh`
--
ALTER TABLE `tbl_kh`
  MODIFY `STT` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `tbl_sukien`
--
ALTER TABLE `tbl_sukien`
  MODIFY `mask` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28092023;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
