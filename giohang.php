
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./giohang.css"> 
    <link href="stylesheet" href="./fontawesome-free-5.15.4-web/">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

</head>
<body>
  <div class="box">
      <?php require './menu.php';?>
            </div>  
            <div class="small-container cart-page">
              <table>
                  <tr>
                      <th>Ảnh</th>
                      <th>Tên Địa Điểm</th>
                      <th>Mã Tour</th>
                      <th>Thời gian</th>
                      <th>Giá</th>
                  </tr>
                  <?php
                            $matour = $_GET['matour'];

                            $sql = "SELECT * FROM tbl_travel WHERE matour=$matour";
                          
                            $res = mysqli_query($conn, $sql);

                            if($res==TRUE)
                            {
                                $count = mysqli_num_rows($res);

                                if($count==1)
                                {
                                    $row = mysqli_fetch_assoc($res);

                                    $madm = $row['madm'];
                                    $tentour = $row['tentour'];
                                    $thongtin = $row['thongtin'];
                                    $gia = $row['gia'];
                                    $matour = $row['matour'];
                                    $thoigian = $row['thoigian'];
                                    $image_name = $row['image'];
                                    $image1 = $row['image1'];
                                    $image2 = $row['image2'];
                                    $image3 = $row['image3'];
                                }
                                
                                else
                                {
                                    die();
                                }
                            }
                          ?>
                  <tr>
                      <td>
                         
                              <img src="<?php echo SITEURL; ?>images/<?php echo $image_name; ?>">
                      </td>
                      <td><?php echo $tentour;?></td>
                      <td><?php echo $matour;?></td>
                      <td><?php echo $thoigian;?></td>
                      <td><?php echo $gia;?> VNĐ</td>
              <div class="total-price">
                  <table >
                      <tr>
                          <td></td>

                      </tr>
                      <tr>
                          <td></td>
                          <td></td>
                      </tr>
                  </table>
                </div>
                  <div class="bt">
                          <button  type="button"><a href="<?php echo SITEURL; ?>thanhtoan.php?matour=<?php echo $matour; ?>">Đặt Lịch</a></button>
                          <button  type="button"><a href="<?php echo SITEURL;?>trangchu.php">Thoát</a></button> 
                        </div>   
          </div>
            <div class="footer">
                <div>
                  <div>
                    <a href="#"><h4>TÌM KIẾM VÀ ĐẶT CHỖ MIỀN BẮC</h4></a>
                    <hr align="left" width="50">
                    <a href="#"><p>CÁC KÌ NGHỈ BÊN BỜ BIỂN</p></a>
                    <a href="#"><p>NGÀY CUỐI TUẦN</p></a>
                    <a href="#"><p>DU LỊCH </p></a>
                    
                  </div>
                  <div>
                    <a href="#"><h4>TÌM KIẾM VÀ ĐẶT CHỖ MIỀN TRUNG</h4></a>
                    <hr align="left" width="50">
                    <a href="#"><p>CÁC KÌ NGHỈ BÊN BỜ BIỂN</p></a>
                    <a href="#"><p>NGÀY CUỐI TUẦN</p></a>
                    <a href="#"><p>DU LỊCH </p></a>
                    
                  </div>
                  <div>
                    <a href="#"><h4>TÌM KIẾM VÀ ĐẶT CHỖ MIỀN NAM</h4></a>
                    <hr align="left" width="50">
                    <a href="#"><p>CÁC KÌ NGHỈ BÊN BỜ BIỂN</p></a>
                    <a href="#"><p>NGÀY CUỐI TUẦN</p></a>
                    <a href="#"><p>DU LỊCH </p></a>
                    
                  </div>
                  <div>
                    <a href="#"><h4>TÌM KIẾM VÀ ĐẶT CHỖ THEO TOUR</h4></a>
                    <hr align="left" width="50">
                    <a href="#"><p>CÁC KÌ NGHỈ BÊN BỜ BIỂN</p></a>
                    <a href="#"><p>NGÀY CUỐI TUẦN</p></a>
                    <a href="#"><p>DU LỊCH </p></a>
                    
                  </div>
                  </div>
                  <hr width="100%" color="white">
         
                    <div class="follow">
                      <h4>THEO DÕI CHÚNG TÔI</h4>
                      <a href="#"><i class="fab fa-facebook-f"></i></a>
                      <a href="#"><i class="fab fa-youtube"></i></a>
                      <a href="#"><i class="fab fa-instagram"></i></a>
                    </div>
                    <video width="400" height="400" autoplay loop>
                      <source src="img/minhtus.mp4">
                    </video>
                </div>
              </div>
              
                  <div class="copy-right">
                    <a href="#"><i class=" fa fa-copyright"></i>2021 by Minhtus</a>
                  </div>
          </div>
        </body>
        </html>