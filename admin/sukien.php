
<html>
    <head>
        <link rel="stylesheet" href="./qltour.css">
        <link rel="stylesheet" href="./fontawesome-free-5.15.4-web/css/all.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">
    </head>
    <body>
      <div id="viewport">
          <!-- Sidebar -->
          <div id="sidebar">
            <header>
              <a href="#">Quản lí website</a>
            </header>
            <ul class="nav">
            <li>
                  <a href="http://localhost:80/tour-order/admin/trangchu.php">
                    <i class="fas fa-home"></i>Quản lý admin
                  </a>
                </li>
                <li>
                  <a href="http://localhost:80/tour-order/admin/quanlytour.php">
                    <i class="fas fa-tasks"></i>Quản lý du lịch  
                  </a>
                </li>
                <li>
                  <a href="http://localhost:80/tour-order/admin/quanlydlbien.php">
                  <i class="fas fa-tasks"></i> Quản lý du lịch biển
                  </a>
                  <ul class="travel-sea">
                    <li><a href="http://localhost:80/tour-order/admin/ql-bb.php">Du lịch bờ biển</a></li>
                    <li><a href="http://localhost:80/tour-order/admin/ql-dt.php">Du lịch du thuyền</a></li>
                  </ul>
                </li>
                <li>
                  <a href="http://localhost:80/tour-order/admin/quanlyhd.php">
                    <i class="fas fa-tasks"></i>Quản Lý Hóa Đơn
                  </a>
                </li>
                <li>
                  <a href="http://localhost:80/tour-order/admin/sukien.php">
                    <i class="far fa-calendar-alt"></i> Quản Lí Sự Kiện
                  </a>
                </li>
                <li>
                  <a href="http://localhost:80/tour-order/admin/qldanhmuc.php">
                    <i class="far fa-calendar-alt"></i> Quản Lí Danh Mục
                  </a>
                </li>
                <li>
                  <a href="http://localhost:80/tour-order/admin/qlkh.php">
                    <i class="far fa-calendar-alt"></i> Quản Lí Khách Hàng
                  </a>
                </li>
                <li>
                  <a href="http://localhost:80/tour-order/admin/tk.php">
                    <i class="fas fa-chart-line"></i> Thống Kê
                  </a>
                </li>
                <li>
                  <a href="http://localhost:80/tour-order/admin/login.php">
                    <i class="fas fa-sign-out-alt"></i>Đăng xuất
                  </a>
                </li>
            </ul>
          </div>
        </div>
          <div id="content">
            <nav class="navbar navbar-default">
              <div class="container-fluid active">
                  <div class="header">
                  <div class="manage-admin">
                      <div>
                          <div style="margin-left:150px; margin-top: 100px;">
                              <h3>Quản lý sự kiện</h3>
                          </div>
                      </div>
                      <div>
                          <button id="btn" ><a  href="http://localhost:80/tour-order/admin/add-even.php">Thêm sự kiện</a></button>
                      </div>
                      <table border="1">
                          <tr>
                              
                              <th>Mã sự kiện</th>
                              <th>Mã tour</th>
                              <th>Tên sự kiện</th>
                              <th>ND</th>
                              <th>Quản lí</th>
                          </tr>        
                          <?php
                          require './config/constants.php';
                          $sql = "SELECT * FROM tbl_sukien";
                            $res = mysqli_query($conn, $sql);
                            if($res==TRUE) 
                            {
                                $count = mysqli_num_rows($res);
                                $stt=1;
                                if($count>0) 
                                {
                                    while($rows=mysqli_fetch_assoc($res))
                                    {
                                        $matour = $rows['matour'];
                                        $mask = $rows['mask'];
                                        $tensk = $rows['tensk'];
                                        $noidung = $rows['noidung'];
                                        ?>

                          <tr>
                              
                              <td><?php echo $mask; ?></td>
                              <td><?php echo $matour; ?></td>
                              <td><?php echo $tensk; ?></td>
                              <td><?php echo $noidung;?></td>
                              <td>
                                 <button id="btnUpdate" onclick="myUpdate()"><a href="<?php echo SITEURL; ?>admin/update-event.php?mask=<?php echo $mask;?>">Sửa</a></button>
                                 <button id="btnDelete" onclick="myDelete()"><a href="<?php echo SITEURL; ?>admin/delete-event.php?mask=<?php echo $mask; ?>">Xóa</a></button>

                              </td>
                          </tr>
                          <?php
                                    }
                                }
                                else
                                {
                                }
                            }
                          ?>
                      </table>
                  </div>
              </div>
            </nav>
          </div>
    </body>
</html>