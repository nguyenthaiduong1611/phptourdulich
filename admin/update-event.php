<?php require './config/constants.php'; ?>
<html>
    <head>
        <link rel="stylesheet" href="add-tours.css">
        <link rel="stylesheet" href="./fontawesome-free-5.15.4-web/css/all.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">
    </head>
    <body>
        <div id="viewport">
            <!-- Sidebar -->
             <div id="sidebar">
              <header>
                <a href="#">Quản lí website</a>
              </header>
              <ul class="nav">
                <li>
                  <a href="<?php echo SITEURL;?>admin/trangchu.php">
                    <i class="fas fa-home"></i>Quản lý admin
                  </a>
                </li>
                <li>
                  <a href="<?php echo SITEURL;?>admin/quanlytour.php">
                    <i class="fas fa-tasks"></i>Quản lý du lịch  
                  </a>
                </li>
                <li>
                  <a href="http://localhost:80/tour-order/admin/quanlydlbien.php">
                  <i class="fas fa-tasks"></i> Quản lý du lịch biển
                  </a>
                  <ul class="travel-sea">
                    <li><a href="http://localhost:80/tour-order/admin/ql-bb.php">Du lịch bờ biển</a></li>
                    <li><a href="http://localhost:80/tour-order/admin/ql-dt.php">Du lịch du thuyền</a></li>
                  </ul>
                </li>
                <li>
                  <a href="<?php echo SITEURL;?>admin/quanlyhd.php">
                    <i class="fas fa-tasks"></i>Quản Lý Hóa Đơn
                  </a>
                </li>
                <li>
                  <a href="<?php echo SITEURL;?>admin/sukien.php">
                    <i class="far fa-calendar-alt"></i> Quản Lí Sự Kiện
                  </a>
                </li>
                <li>
                  <a href="<?php echo SITEURL;?>admin/qldanhmuc.php">
                    <i class="far fa-calendar-alt"></i> Quản Lí Danh Mục
                  </a>
                </li>
                <li>
                  <a href="http://localhost:80/tour-order/admin/qlkh.php">
                    <i class="far fa-calendar-alt"></i> Quản Lí Khách Hàng
                  </a>
                </li>
                <li>
                  <a href="<?php echo SITEURL;?>admin/tk.php">
                    <i class="fas fa-chart-line"></i> Thống Kê
                  </a>
                </li>
                <li>
                  <a href="<?php echo SITEURL;?>admin/login.php">
                    <i class="fas fa-sign-out-alt"></i>Đăng xuất
                  </a>
                </li>
              </ul>
            </div> 
          </div> 
          <div id="content">
            <nav class="navbar navbar-default">
              <div class="container-fluid active">
                  <div class="header">
                  <div class="manage-admin">
                      <div>
                          <div style="margin-left:150px; margin-top: 100px;">
                              <h3>Update sự kiện</h3>
                          </div>
                      </div>
                      <?php
                            $mask = $_GET['mask'];

                            $sql = "SELECT * FROM tbl_sukien WHERE mask=$mask";
                          
                            $res = mysqli_query($conn, $sql);

                            if($res==TRUE)
                            {
                                $count = mysqli_num_rows($res);

                                if($count==1)
                                {
                                    $row = mysqli_fetch_assoc($res);

                                    $mask = $row['mask'];
                                    $matour = $row['matour'];
                                    $tensk = $row['tensk'];
                                    $noidung = $row['noidung'];
                                    
                                }
                                else
                                {
                                    header('location:'.SITEURL.'admin/sukien.php');
                                }
                            }
                          ?>
                      <div>
                          <form action="" method="POST">
                              <lable>Mã sự kiện</lable><br>
                              <input type="text" name="mask" id="matour" value="<?php echo $mask?>"><br>
                              <lable>Mã tour</lable><br>
                              <input type="text" name="matour" id="madm" value="<?php echo $matour; ?>"><br>
                              <lable>Tên sự kiện</lable><br>
                              <input type="text" name="tensk" id="tentour" value="<?php echo $tensk;?>"><br>
                              <lable>Nội dung</lable><br>
                              <input type="text" name="noidung" id="thongtin" value="<?php echo $noidung; ?>"><br>
                              <input type="submit" name="save" value="Thêm" id="save">
                          </form>
                      </div>
                  </div>
              </div>
            </nav>
          </div>
    </body>
</html>

<?php
    if(isset($_POST['save']))
    {
        $matour = $_POST['matour'];
        $mask = $_POST['mask'];
        $tensk = $_POST['tensk'];
        $noidung = $_POST['noidung'];

        $sql1 =  "UPDATE tbl_sukien SET
            tensk ='$tensk',
            noidung = '$noidung',
            matour = '$matour'
            WHERE mask = '$mask'
        ";
        $res1= mysqli_query($conn, $sql1) ;
        if($res1==TRUE)
        {
            header('location:'.SITEURL.'admin/sukien.php');
        }
        else
        {
          die();
        }
    }
    
?>