<?php require './config/constants.php'; ?>
<html>
    <head>
        <link rel="stylesheet" href="add-tours.css">
        <link rel="stylesheet" href="./fontawesome-free-5.15.4-web/css/all.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">
    </head>
    <body>
        <div id="viewport">
            <!-- Sidebar -->
             <div id="sidebar">
              <header>
                <a href="#">Quản lí website</a>
              </header>
              <ul class="nav">
                <li>
                  <a href="<?php echo SITEURL;?>admin/trangchu.php">
                    <i class="fas fa-home"></i>Quản lý admin
                  </a>
                </li>
                <li>
                  <a href="<?php echo SITEURL;?>admin/quanlytour.php">
                    <i class="fas fa-tasks"></i>Quản lý du lịch  
                  </a>
                </li>
                <li>
                  <a href="http://localhost:80/tour-order/admin/quanlydlbien.php">
                  <i class="fas fa-tasks"></i> Quản lý du lịch biển
                  </a>
                  <ul class="travel-sea">
                    <li><a href="http://localhost:80/tour-order/admin/ql-bb.php">Du lịch bờ biển</a></li>
                    <li><a href="http://localhost:80/tour-order/admin/ql-dt.php">Du lịch du thuyền</a></li>
                  </ul>
                </li>
                <li>
                  <a href="<?php echo SITEURL;?>admin/quanlyhd.php">
                    <i class="fas fa-tasks"></i>Quản Lý Hóa Đơn
                  </a>
                </li>
                <li>
                  <a href="<?php echo SITEURL;?>admin/sukien.php">
                    <i class="far fa-calendar-alt"></i> Quản Lí Sự Kiện
                  </a>
                </li>
                <li>
                  <a href="<?php echo SITEURL;?>admin/qldanhmuc.php">
                    <i class="far fa-calendar-alt"></i> Quản Lí Danh Mục
                  </a>
                </li>
                <li>
                  <a href="http://localhost:80/tour-order/admin/qlkh.php">
                    <i class="far fa-calendar-alt"></i> Quản Lí Khách Hàng
                  </a>
                </li>
                <li>
                  <a href="<?php echo SITEURL;?>admin/tk.php">
                    <i class="fas fa-chart-line"></i> Thống Kê
                  </a>
                </li>
                <li>
                  <a href="<?php echo SITEURL;?>admin/login.php">
                    <i class="fas fa-sign-out-alt"></i>Đăng xuất
                  </a>
                </li>
              </ul>
            </div> 
          </div> 
          <div id="content">
            <nav class="navbar navbar-default">
              <div class="container-fluid active">
                  <div class="header">
                  <div class="manage-admin">
                      <div>
                          <div style="margin-left:150px; margin-top: 100px;">
                              <h3>Thêm Địa Điểm du lịch du thuyền</h3>
                          </div>
                      </div>
                      <div>
                          <form action="" method="POST" enctype="multipart/form-data">
                              <lable>Mã tour</lable><br>
                              <input type="text" name="matour" id="matour"><br>
                              <lable>Mã danh mục</lable><br>
                              <input type="text" name="madm" id="madm"><br>
                              <lable>Tên Địa Điểm</lable><br>
                              <input type="text" name="tentour" id="tentour"><br>
                              <lable>Thông tin</lable><br>
                              <textarea name="thongtin" id="thongtin"></textarea><br>
                              <lable>Thời gian</lable><br>
                              <input type="text" name="thoigian" id="tentour"><br>
                              <lable>Ảnh</lable><br>
                              <input type="file" name="image" id="image"><br>
                              <lable>Ảnh</lable><br>
                              <input type="file" name="image1" id="image"><br>
                              <lable>Ảnh</lable><br>
                              <input type="file" name="image2" id="image"><br>
                              <lable>Ảnh</lable><br>
                              <input type="file" name="image3" id="image"><br>
                              <lable>Giá</lable><br>
                              <input type="number" name="gia" id="gia"><br><br>
                              <input type="submit" name="save" id="save">
                          </form>
                      </div>
                  </div>
              </div>
            </nav>
          </div>
    </body>
</html>

<?php
    if(isset($_POST['save']))
    {
        $matour = $_POST['matour'];
        $madm = $_POST['madm'];
        $tentour = $_POST['tentour'];
        $thongtin = $_POST['thongtin'];
        $gia = $_POST['gia'];
        $thoigian = $_POST['thoigian'];

        if(isset($_FILES['image']['name']))
        {
            $image_name = $_FILES['image']['name'];

            if($image_name!="")
            {
                $ext = end(explode('.',$image_name));

                $image_name = "tour-name".rand(0000, 9999).".".$ext;

                $src = $_FILES['image']['tmp_name'];

                $dst = "../images/".$image_name;

                $upload = move_uploaded_file($src, $dst);

                if($upload==TRUE)
                {
                  header("location:".SITEURL.'admin/ql-bb.php');
                }
            }
        }
        else
        {
            $image_name = "";
        }

        if(isset($_FILES['image1']['name']))
        {
            $image1 = $_FILES['image1']['name'];

            if($image1!="")
            {
                $ext = end(explode('.',$image1));

                $image1 = "tour-name".rand(0000, 9999).".".$ext;

                $src = $_FILES['image1']['tmp_name'];

                $dst = "../images/".$image1;

                $upload = move_uploaded_file($src, $dst);

                if($upload==TRUE)
                {
                  header("location:".SITEURL.'admin/ql-bb.php');
                }
            }
        }
        else
        {
            $image1 = "";
        }

        if(isset($_FILES['image2']['name']))
        {
            $image2 = $_FILES['image2']['name'];

            if($image2!="")
            {
                $ext = end(explode('.',$image2));

                $image2 = "tour-name".rand(0000, 9999).".".$ext;

                $src = $_FILES['image2']['tmp_name'];

                $dst = "../images/".$image2;

                $upload = move_uploaded_file($src, $dst);

                if($upload==TRUE)
                {
                  header("location:".SITEURL.'admin/ql-bb.php');
                }
            }
        }
        else
        {
            $image2 = "";
        }

        if(isset($_FILES['image3']['name']))
        {
            $image3 = $_FILES['image3']['name'];

            if($image3!="")
            {
                $ext = end(explode('.',$image3));

                $image3 = "tour-name".rand(0000, 9999).".".$ext;

                $src = $_FILES['image3']['tmp_name'];

                $dst = "../images/".$image3;

                $upload = move_uploaded_file($src, $dst);

                if($upload==TRUE)
                {
                  header("location:".SITEURL.'admin/ql-bb.php');
                }
            }
        }
        else
        {
            $image3 = "";
        }

        $sql =  "INSERT INTO tbl_dt SET
            matour = $matour,
            madm = $madm,
            tentour ='$tentour',
            thongtin = '$thongtin',
            thoigian = '$thoigian',
            image = '$image_name',
            image1 = '$image1',
            image2 = '$image2',
            image3 = '$image3',
            gia = '$gia'
        ";
        $res2 = mysqli_query($conn, $sql) ;
        if($res2==TRUE)
        {
            die();
        }
    }
    
?>